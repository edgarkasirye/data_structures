Rails.application.routes.draw do
  resources :courses
  devise_for :admin_users, ActiveAdmin::Devise.config
  ActiveAdmin.routes(self)
  root to: 'home#index'
  match '/course_outline' => "courses#index", via: ['get']
  match '/our_team' => "team#index", via: ['get']
  match '/interactions' => "interactions#index", via: ['get']
  match '/contact_us' => "contact#index", via: ['get']
  match '/fibonacci_and_factorial' => "computations#index", via: ['get']
  match '/fibonacci_and_factorial_computation' => "computations#fibonacci_and_factorial", via: ['post']
  match '/tower_of_hanoi' => "computations#index_hanoi", via: ['get']
  match '/tower_of_hanoi_computation' => "computations#tower_of_hanoi", via: ['post']
  match '/sorting' => "sorting#index", via: ['get']
  match '/sorted' => "sorting#sorted", via: ['post']
end

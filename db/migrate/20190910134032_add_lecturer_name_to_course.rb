class AddLecturerNameToCourse < ActiveRecord::Migration[5.2]
  def change
    add_column :courses, :lecturer_name, :string
  end
end

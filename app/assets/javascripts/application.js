// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, or any plugin's
// vendor/assets/javascripts directory can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// compiled file. JavaScript code in this file should be added after the last require_* statement.
//
// Read Sprockets README (https://github.com/rails/sprockets#sprockets-directives) for details
// about supported directives.
//= require rails-ujs
//= require activestorage
//= require crystal/jquery.min
//= require crystal/jquery.stellar.min
//= require crystal/modernizr
//= require crystal/bootstrap.min
//= require crystal/scrollIt.min
//= require crystal/nav-menu
//= require crystal/easy.responsive.tabs
//= require crystal/owl.carousel
//= require crystal/jquery.counterup.min
//= require crystal/jquery.stellar.min
//= require crystal/waypoints.min
//= require crystal/tabs.min
//= require crystal/countdown
//= require crystal/jquery.magnific-popup.min
//= require crystal/isotope.pkgd.min
//= require crystal/wow
//= require crystal/map
//= require crystal/main

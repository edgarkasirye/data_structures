class SortingController < ApplicationController
  skip_before_action :verify_authenticity_token
  include Computations
  def index
  end

  def sorted
    @arr = []
    @unsorted = []
    @method = params[:sort_method]
    number = (params[:number]).to_i
    for i in 1..number
      i = rand(1000)
      @arr << i
      @unsorted << i
    end
    if @method == "Bubble sort"
      @result = bubble_sort(@arr)
    elsif @method == "Insertion sort"
      @result = insertion_sort(@arr)
    elsif @method == "Selection sort"
      @result = selection_sort(@arr)
    elsif @method == "Quick sort"
      @result = quick_sort(@arr)
    elsif @method == "Merge sort"
      @result = merge_sort(@arr)
    else
      @arr = []
      @unsorted = []
    end
  end
end

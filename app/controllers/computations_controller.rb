class ComputationsController < ApplicationController
  skip_before_action :verify_authenticity_token
  include Computations

  def index
  end

  def index_hanoi
  end

  def fibonacci_and_factorial
    #method to execute the fibonacci  and factorial functions
    @method = params[:method]
    @number = (params[:number]).to_i
    if @method == "Fibonacci"
      @result = fibonacci_computation(@number)
    end
    if @method == "Factorial"
      @result = factorial_computation(@number)
    end
  end
end

ActiveAdmin.register Course do
  permit_params :topic, :description, :lecturer_name
end
